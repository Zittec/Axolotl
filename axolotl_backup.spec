# -*- mode: python -*-

block_cipher = None


a = Analysis(['axolotl.pyw'],
             pathex=['C:\\Users\\Grupo Helix\\Documents\\Zittec\\Axolotl'],
             binaries=[],
             datas=[('axolotl.ui', '.'),('ext/wordcount.py','ext/'),('ext/table.py','ext/'),('ext/find.py','ext/'),('ext/datetime.py','ext/'),('icons/align-center.png','icons/'),('icons/align-justify.png','icons/'),('icons/align-left.png','icons/'),('icons/align-right.png','icons/'),('icons/bold.png','icons/'),('icons/bullet.png','icons/'),('icons/calender.png','icons/'),('icons/ayuda.png','icons/'),('icons/copy.png','icons/'),('icons/count.png','icons/'),('icons/cut.png','icons/'),('icons/dedent.png','icons/'),('icons/document-export.png','icons/'),('icons/document-import.png','icons/'),('icons/document-send.png','icons/'),('icons/edit-find-replace.png','icons/'),('icons/edit-select-all.png','icons/'),('icons/find.png','icons/'),('icons/font-color.png','icons/'),('icons/gitlab.png','icons/'),('icons/icon.png','icons/'),('icons/indent.png','icons/'),('icons/insert-image.png','icons/'),('icons/insert-link.png','icons/'),('icons/italic.png','icons/'),('icons/new.png','icons/'),('icons/new.png','icons/'),('icons/open.png','icons/'),('icons/package-install.png','icons/'),('icons/package-remove.png','icons/'),('icons/package-upgrade.png','icons/'),('icons/paste.png','icons/'),('icons/preview.png','icons/'),('icons/print.png','icons/'),('icons/redo.png','icons/'),('icons/reportar.png','icons/'),('icons/salir.png','icons/'),('icons/save.png','icons/'),('icons/strike.png','icons/'),('icons/underline.png','icons/'),('icons/undo.png','icons/'),('images/splash.png','images/'),('images/logo500.png','images/')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='axolotl',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False , icon='images/Axolotl_logo_icon.ico')
