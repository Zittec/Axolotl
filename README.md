## Sinopsis

Axolotl es una interfaz gráfica de usuario (GUI) desarrollada por y para la comunidad Mozilla México (MozillaMX) para poder publicar directamente desde tu computadora en la página oficial de la comunidad. Está desarrollada en PyQT5 y tiene un ejecutable para Windows aunque con un poco de ingenio podría ser portado a Linux.

## Instalación

**Si todo sale bien:**
Simplemente ejecuta el archivo axolotl.exe

**Si te muestra el error:**
>"No se encuentra el punto de entrada del procedimiento ucrtbase.terminate en la biblioteca de vínculos dinámicos api-ms-win-crt-runtime-l1-1-0.dll."

Eso quiere decir que te hace falta "Microsoft Visual C++ 2015 Redistributable Update 3RC" el cual puedes descargar de este link [https://www.microsoft.com/es-es/download/details.aspx?id=52685](https://www.microsoft.com/es-es/download/details.aspx?id=52685). Existen dos posibles descargas, una de 32Bits (x86) y otra de 64Bits (x64). _Si no tienes idea de qué es eso de 32 y  64 bits trata de instalar uno y si no te deja prueba con el otro, no te preocupes ya que de no ser el indicado no te dejará instalarlo._
No lo incluimos en este repositorio por aquello de los problemas de licencias... _¿Alguien sabe si es legal ponerlo aquí?_

## Código

No hay necesidad de alterar o ejecutar ningún código más que para adaptarlo a tus necesidades en caso de querer desarrollar un proyecto alterno. Para el uso que fue planteado simplemente hay que ejecutar el archivo axolotl.exe el cual tiene una interfaz gráfica.

### Generación del archivo **axolotl.exe**

En el caso de que desees generar el archivo axolotl.exe por ti mism@, el siguiente comando generará el archivo axolotl.exe en la carpeta dist. Si no existe la carpeta dist, la creará.
```
	pyinstaller.exe axolotl.spec
```


### Generación del archivo **axolotl.spec** base (sin dependencias)
```
	pyinstaller.exe --onefile --windowed --icon axolotl.ico axolotl.pyw
```

### Carpetas y archivos

	|	axolotl.exe 		: Ejecutable de Axolotl (standalone).
	|	LICENSE 			: Contiene la licencia bajo la que está liberado Axolotl.
	|	README.md 			: Este archivo
	|	axolotl.spec		: Es el archivo con la configuración para generar axolotl.exe mediante el comando "pyinstaller.exe axolotl.spec"
	|	axolotl_backup.spec	: Es una copia de respaldo del archivo axolotl.spec modificado para incluir las dependencias de axolotl. Existe sólo para prevenir la sobreescritura del mismo por el comando "pyinstaller.exe axolotl.pyw" el cual generará un archivo axolotl.exe no ejecutable por no contener las dependencias necesarias.
	|	README.md 			: Este archivo
	|	Gittex 			: Mini script para respaldar automáticamente los cambios (ver proyecto Gittex)
	ext 					: Directorio de los archivos de python de los que depende axolotl.pyw
	icons 					: Directorio con íconos de la interfaz gráfica de axolotl.pyw
	images 					: Directorio con el logotipo de Axolotl así como de su splash y el ícono, así como su archivo origen editable en formato svg.
	git_icons				: Íconos de Gittex (ver proyecto Gittex)

### Requerimientos y configuración utilizada para desarrollar, ejecutar, compilar y ejecutar el archivo compilado

	python 3.6.4
	pip3 9.0.1
	SIP 4.19.6
	PyQt5 5.9.x (Instalado via pip3)
	PyInstaller 3.3.1
	Microsoft Visual C++ 2015 Redistributable Update 3RC (para ejecutar axolotl.exe)
	Editor de texto
	Inkscape (Editor de gráficos vectoriales SVG) Usado para crear el logotipo y sus aplicaciónes
	GIMP (Editor de imágenes) Usado para convertir el logo en ícono

## Motivación

El sitio oficial de la comunidad está desarrollado bajo Jekill+Github por lo que fue necesario implementar una interfaz gráfica que permitiera editar y publicar fácilmente los contenidos así como subirlo al repositorio correspondiente y todo esto sin tener que ser un experto en tecnologías por lo que optamos por generar nuestro Axolotl.

## Colaboradores

Lucio Chávez, Odín Mojica, Elesban Landero y toda la comunidad Mozilla México

## Licencia

Este proyecto está licenciado bajo la licencia de Software Libre GPLv3.
